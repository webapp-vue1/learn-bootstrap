module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '~cs62160315/ln_bootstrap/'
    : '/'
}